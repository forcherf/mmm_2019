&MULTIPLE_FORCE_EVALS
  FORCE_EVAL_ORDER 2 3
  MULTIPLE_SUBSYS T
&END
&FORCE_EVAL
  METHOD MIXED
  &MIXED
    MIXING_TYPE GENMIX
    GROUP_PARTITION 2 22
    &GENERIC
      ERROR_LIMIT 1.0E-10
      MIXING_FUNCTION E1+E2
      VARIABLES E1 E2
    &END
    &MAPPING
       &FORCE_EVAL_MIXED
         &FRAGMENT 1
            1 222
         &END
         &FRAGMENT 2
            223 1062
         &END
       &END
       &FORCE_EVAL 1
         DEFINE_FRAGMENTS 1 2
       &END
       &FORCE_EVAL 2
         DEFINE_FRAGMENTS 1
       &END
    &END
  &END
  &SUBSYS
    &CELL
	ABC 62.448455 20.602647 40
    &END CELL
    &TOPOLOGY
      COORD_FILE_NAME ./all.xyz
      COORDINATE XYZ
      CONNECTIVITY OFF
    &END TOPOLOGY
  &END SUBSYS
&END
&FORCE_EVAL
  METHOD FIST
  &MM
   &FORCEFIELD
     &SPLINE
       EPS_SPLINE 1.30E-5
       EMAX_SPLINE 0.8
     &END
     &CHARGE
       ATOM Au
       CHARGE 0.0
     &END CHARGE
     &CHARGE
       ATOM H
       CHARGE 0.0
     &END CHARGE
     &CHARGE
       ATOM C
       CHARGE 0.0
     &END CHARGE
     &CHARGE
       ATOM O
       CHARGE 0.0
     &END CHARGE
     &CHARGE
       ATOM N
       CHARGE 0.0
     &END CHARGE
     &NONBONDED
       &GENPOT
          atoms Au C
          FUNCTION A*exp(-av*r)+B*exp(-ac*r)-C/(r^6)/( 1+exp(-20*(r/R-1)) )
          VARIABLES r
          PARAMETERS A av B ac C R
          VALUES 4.13643 1.33747 115.82004 2.206825 113.96850410723008483218 5.84114 
          RCUT  15
       &END GENPOT
       &GENPOT
          atoms Au O
          FUNCTION A*exp(-av*r)+B*exp(-ac*r)-C/(r^6)/( 1+exp(-20*(r/R-1)) )
          VARIABLES r
          PARAMETERS A av B ac C R
          VALUES 4.13643 1.33747 115.82004 2.206825 113.96850410723008483218 5.84114 
          RCUT  15
       &END GENPOT
       &GENPOT
          atoms Au N
          FUNCTION A*exp(-av*r)+B*exp(-ac*r)-C/(r^6)/( 1+exp(-20*(r/R-1)) )
          VARIABLES r
          PARAMETERS A av B ac C R
          VALUES 4.13643 1.33747 115.82004 2.206825 113.96850410723008483218 5.84114 
          RCUT  15
       &END GENPOT
       &GENPOT
          atoms Au H
          FUNCTION A*exp(-av*r)+B*exp(-ac*r)-C/(r^6)/( 1+exp(-20*(r/R-1)) ) 
          VARIABLES r
          PARAMETERS A av B ac C R
          VALUES 0.878363 1.33747 24.594164 2.206825 32.23516124268186181470 5.84114
          RCUT 15
       &END GENPOT
       &LENNARD-JONES
         atoms C H
         EPSILON 0.0
         SIGMA 3.166
         RCUT  15
       &END LENNARD-JONES
        &LENNARD-JONES
         atoms H H
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
        &LENNARD-JONES
         atoms H N
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms C C
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms C O
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms C N
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms N N
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms O H
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms O N
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &LENNARD-JONES
         atoms O O
         EPSILON 0.0
         SIGMA 3.166
         RCUT 15
       &END LENNARD-JONES
       &EAM
         atoms Au Au 
         PARM_FILE_NAME ./Au.pot
       &END EAM
     &END NONBONDED
   &END FORCEFIELD
   &POISSON
     &EWALD
       EWALD_TYPE none
     &END EWALD
   &END POISSON
  &END
  &SUBSYS
    &CELL
	ABC  62.448455 20.602647 40
    &END CELL
    &TOPOLOGY
      COORD_FILE_NAME ./all.xyz
      COORDINATE XYZ
      CONNECTIVITY OFF
    &END TOPOLOGY
  &END SUBSYS
&END
&FORCE_EVAL
  METHOD Quickstep
  &DFT
    &QS
      METHOD DFTB
      EXTRAPOLATION ASPC
      EXTRAPOLATION_ORDER 3
       &DFTB
          SELF_CONSISTENT    T
          DISPERSION         T
          ORTHOGONAL_BASIS   F
          DO_EWALD           F
          &PARAMETER
           PARAM_FILE_PATH  ./scc
           PARAM_FILE_NAME  scc_parameter
           UFF_FORCE_FIELD  ./uff_table
          &END PARAMETER
      &END DFTB
    &END QS
    &SCF
      MAX_SCF 30
      SCF_GUESS RESTART
      EPS_SCF 1.0E-6
      &OT
        PRECONDITIONER  FULL_SINGLE_INVERSE
        MINIMIZER  CG
      &END
      &OUTER_SCF
        MAX_SCF 20
        EPS_SCF 1.0E-6
      &END
      &PRINT
        &RESTART
          &EACH
            QS_SCF 0
            GEO_OPT 1
          &END
          ADD_LAST NUMERIC
          FILENAME RESTART
        &END
        &RESTART_HISTORY OFF
        &END
      &END
    &END SCF
    &XC
      &XC_FUNCTIONAL PBE
      &END XC_FUNCTIONAL
        &VDW_POTENTIAL
          DISPERSION_FUNCTIONAL PAIR_POTENTIAL
          &PAIR_POTENTIAL
           TYPE DFTD2
           # According to Carlo, these parameters are from a paper by Levy
           #          ATOMPARM Au 95.3975 3.1368
           #  Screend parameters from
           #  Ruiz, V. G.; Liu, W.; Zojer, E.; Scheffler, M.; Tkatchenko, A. PRL 2012, 108, 146103.
           #          ATOMPARM Ag 122  2.57
           ATOMPARM Au 134  2.91
           REFERENCE_FUNCTIONAL PBE
           VERBOSE_OUTPUT TRUE
          &END PAIR_POTENTIAL
        &END VDW_POTENTIAL
    &END XC
  &END DFT
  &SUBSYS
    &CELL
	ABC  62.448455 20.602647 40
    &END CELL
    &TOPOLOGY
      COORD_FILE_NAME ./mol.xyz
      COORDINATE xyz
       &CENTER_COORDINATES
       &END 
    &END
  &END SUBSYS
&END FORCE_EVAL
&GLOBAL
# EXTENDED_FFT_LENGTHS
  PRINT_LEVEL LOW
  PROJECT PROJ
  RUN_TYPE GEO_OPT
&END GLOBAL
&MOTION
    &CONSTRAINT
     &FIXED_ATOMS
      LIST 223..1062
     &END
    &END
  &GEO_OPT  
    OPTIMIZER LBFGS
    MAX_FORCE 0.0003
    MAX_ITER 5000
    &LBFGS
     MAX_H_RANK 40     
    &END
  &END
  &MD
    ENSEMBLE NVT
    STEPS 10000
    TEMPERATURE  100
    &THERMOSTAT
	TYPE NOSE
	&NOSE
	&END
    &END
   &END MD
   &PRINT
    &TRAJECTORY
	&EACH 
	    MD 10
	&END
    &END
   &END PRINT
&END
!&EXT_RESTART                                                                                  
!  RESTART_FILE_NAME ./HBC-Cu-1.restart                                                        
!&END   
